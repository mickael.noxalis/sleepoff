// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn shutdown_computer() {
    println!("Hello, world!");
    if cfg!(target_os = "windows") {
        std::process::Command::new("shutdown").arg("/s").arg("/f").arg("/t").arg("0").output().unwrap();
    } else if cfg!(target_os = "macos") || cfg!(target_os = "linux") {
        std::process::Command::new("/usr/sbin/shutdown").arg("-h").arg("now").output().unwrap();
    }
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![shutdown_computer])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
