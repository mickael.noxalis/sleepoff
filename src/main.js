const { invoke } = window.__TAURI__.tauri;

async function shutdownComputer() {
  // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
  await invoke("shutdown_computer");
}

// Cache DOM elements
const startButton = document.querySelector('#submit');
const stopButton = document.querySelector('#stop');
const minutesInput = document.querySelector('#minutes');
const countdownDisplay = document.querySelector('#countdown');

let interval; // to have access to the interval from other functions

window.addEventListener("DOMContentLoaded", () => {
    startButton.addEventListener('click', startCountdown);
    stopButton.addEventListener('click', stopCountdown);
    switchTheme();
});

function startCountdown(e) {
    e.preventDefault();

    // Disable the start button and enable the stop button
    startButton.disabled = true;
    stopButton.disabled = false;

    let duration = minutesInput.value * 60;  // Convert minutes to seconds
    countdownDisplay.textContent = formatTime(duration);  // Display initial time

    interval = setInterval(() => {
        duration--;

        countdownDisplay.textContent = formatTime(duration);

        if (duration <= 0) {
            stopCountdown();
            shutdownComputer();
        }
    }, 1000);  // 1 second
}

function stopCountdown() {
    clearInterval(interval);
    countdownDisplay.textContent = "0h 00m 00s"
    startButton.disabled = false;
    stopButton.disabled = true;
}

function formatTime(seconds) {
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const secs = seconds % 60;

    return `${hours}h ${minutes}m ${secs < 10 ? '0' : ''}${secs}s`;
}


function switchTheme() {
  let image = document.querySelector(".theme");

  image.addEventListener('click', () => {
      // Basculer le thème
      document.documentElement.classList.toggle("inverted");

      // Basculer l'image
      if (image.src.includes('/assets/sun.svg')) {
          image.src = '/assets/moon.svg';
      } else {
          image.src = '/assets/sun.svg';
      }
  });
}